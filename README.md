# Trexkeys

You shouldn't need to move your arm just to move your cursor. For those who
won't (or can't), there's Trexkeys: keyboard mappings for home row navigation

# Usage

1. Hold the Trexkey (the key directly to the right of your spacebar)
2. Press a home row key to move the cursor
3. Press a key below the home row to both move the cursor, and select text
4. Rawwrrrr!

![Trexkeys Keyboard Layout](resources/trex_keyboard.png)

# Installation

## Windows

Shortcuts on Windows are implemented using [AutoHotKey](http://www.autohotkey.com/).

1. Install AutoHotKey
2. Find the appropriate `.ahk` file inside the `windows` folder for your keyboard layout - if unsure, use "Standard"
3. Double click the AHK to run it - it will show up as an "H" icon in your system tray
4. You're ready to use Trexkeys!
5. Copy the AHK to your Startup folder to have it run automatically on startup (To find the startup folder, press Win+R,
then type `shell:Startup`)

## Mac OS X

Shortcuts on OS X are implemented using [KHD](https://github.com/koekeishiya/khd/).

1. Copy `mac/khdrc` to `~/.khdrc`
2. Install and start KHD using homebrew
3. You're ready to use Trexkeys!

## Linux

Shortcuts on Linux are implemented as a custom keyboard configuration for X11.
This configuration is based on the US keyboard layout, and will only work for keyboards with that layout.

1. Copy `linux/trexkeys.xkb` to `~/.trexkeys.xkb`
2. Copy `linux/Trexkeys.desktop` to `~/.config/autostart/Trexkeys.desktop` for Gnome,
or `~/.kde/share/autostart/Trexkeys.desktop` for KDE.
3. Log out and log back in, and you're ready to use Trexkeys!

# Attributions

* Keyboard template by [Gerbrant](https://commons.wikimedia.org/wiki/User:Gerbrant) is licensed under CC BY-SA 3.0
* Tyrannosaurus icon made by [Freepik](http://www.freepik.com) is licensed under CC BY 3.0
